﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testscript : MonoBehaviour {
    bool controllerCheck = false;
    int a = 0;
    // Use this for initialization
    int n;
	void Start () {
        n = 1;
        ControllerPlugin.initialize(n);

        print(Input.GetJoystickNames().ToString());
        
	}
	void ControllerPrint(int i)
    {
        ControllerPlugin.ControllerUpdate(i);

        print(ControllerPlugin.contGetState(i) + i);
        //while (controllerCheck == true)
        //{
        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_A)
        {
            print("click A " + i);
            // print(a + 1);
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_B)
        {
            print("click B");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_X)
        {
            print("click X");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_Y)
        {
            print("click Y");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_UP)
        {
            print("up");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_DOWN)
        {
            print("down");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_LEFT)
        {
            print("left");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_RIGHT)
        {
            print("right");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_START)
        {
            print("start");
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_BACK)
        {
            print("back");
        }


        print("Left Stick X " + ControllerPlugin.contLeftThumbstickX(i));
        print("Left Stick Y " + ControllerPlugin.contLeftThumbstickY(i));
        print("Right Stick X " + ControllerPlugin.contRightThumbstickX(i));
        print("Right Stick Y " + ControllerPlugin.contRightThumbstickY(i));
        print("Left Trigger " + ControllerPlugin.contLeftTriggerState(i));
        print("Right Trigger " + ControllerPlugin.contRightTriggerState(i));
    }
	// Update is called once per frame
	void Update () {
        ControllerPrint(1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorDemo : EditorWindow
{
    bool eCheckA = false;
    bool eCheckB = false;
    bool echeckX = false;
    bool echeckY = false;
    bool echeckUp = false;
    bool echeckDown = false;
    bool echeckLeft = false;
    bool echeckRight = false;
    bool echeckStart = false;
    bool echeckBack = false;
    bool echeckLShoulder = false;
    bool echeckRShoulder = false;
    public static int n;
    [MenuItem("Window/Editor Demo")]
    static void init()
    {

        EditorWindow window = (EditorDemo)EditorWindow.GetWindow(typeof(EditorDemo));
        window.Show();
        
    }

    void OnInspectorUpdate()
    {
        Repaint(); // Needed, otherwise window will only update when mouse exits or enters window
    }

    // update editor window with controller data
    private void OnGUI()
    {
        eCheckA = EditorGUILayout.Toggle("A", demo.checkA);
        eCheckB = EditorGUILayout.Toggle("B", demo.checkB);
        echeckX = EditorGUILayout.Toggle("X", demo.checkX);
        echeckY = EditorGUILayout.Toggle("Y", demo.checkY);
        echeckUp = EditorGUILayout.Toggle("Up", demo.checkUp);
        echeckDown = EditorGUILayout.Toggle("Down", demo.checkDown);
        echeckLeft = EditorGUILayout.Toggle("Left", demo.checkLeft);
        echeckRight = EditorGUILayout.Toggle("Right", demo.checkRight);
        echeckStart = EditorGUILayout.Toggle("Start", demo.checkStart);
        echeckBack = EditorGUILayout.Toggle("Back", demo.checkBack);
        echeckLShoulder = EditorGUILayout.Toggle("Left Shoulder", demo.checkLShoulder);
        echeckRShoulder = EditorGUILayout.Toggle("Right Shoulder", demo.checkRShoulder);


        EditorGUILayout.TextField("Left Trigger ", demo.LTrigger);
        EditorGUILayout.TextField("Right Trigger ", demo.RTrigger);
        EditorGUILayout.TextField("Right Stick X ", demo.RStickX);
        EditorGUILayout.TextField("Right Stick Y ", demo.RStickY);
        EditorGUILayout.TextField("Left Stick X ", demo.LStickX);
        EditorGUILayout.TextField("Left Stick Y ", demo.LStickY);

        EditorGUILayout.TextField("Active Controller: ", demo.Active);
    }
}
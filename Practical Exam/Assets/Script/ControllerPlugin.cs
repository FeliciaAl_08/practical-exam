﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

// used for comparing values for buttons (returned from plugin)
public class CONSTANTS
{
    public const int GAMEPAD_DPAD_UP = 1;
    public const int GAMEPAD_DPAD_DOWN = 2;
    public const int GAMEPAD_DPAD_LEFT = 4;
    public const int GAMEPAD_DPAD_RIGHT = 8;
    public const int GAMEPAD_START = 16;
    public const int GAMEPAD_BACK = 32;
    public const int GAMEPAD_LEFT_THUMB = 64;
    public const int GAMEPAD_RIGHT_THUMB = 128;
    public const int GAMEPAD_LEFT_SHOULDER = 256;
    public const int GAMEPAD_RIGHT_SHOULDER = 512;
    public const int GAMEPAD_A = 4096;
    public const int GAMEPAD_B = 8192;
    public const int GAMEPAD_X = 16384;
    public const int GAMEPAD_Y = 32768;

}



public class ControllerPlugin : MonoBehaviour
{
    [DllImport("XBOX360Test")]
    public static extern int contGetState(int n);

    [DllImport("XBOX360Test")]
    public static extern void initialize(int n);

    [DllImport("XBOX360Test")]
    public static extern void ControllerUpdate(int n);
    [DllImport("XBOX360Test")]
    public static extern int contLeftTriggerState(int n);

    [DllImport("XBOX360Test")]
    public static extern int contRightTriggerState(int n);

    [DllImport("XBOX360Test")]
    public static extern float contRightThumbstickX(int n);
    [DllImport("XBOX360Test")]
    public static extern float contRightThumbstickY(int n);
    [DllImport("XBOX360Test")]
    public static extern float contLeftThumbstickX(int n);

    [DllImport("XBOX360Test")]
    public static extern float contLeftThumbstickY(int n);

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

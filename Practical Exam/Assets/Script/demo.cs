﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class demo : MonoBehaviour {

    public static bool checkA = false;
    public static bool checkB = false;
    public static bool checkX = false;
    public static bool checkY = false;
    public static bool checkUp = false;
    public static bool checkDown = false;
    public static bool checkLeft = false;
    public static bool checkRight = false;
    public static bool checkStart = false;
    public static bool checkBack = false;
    public static bool checkLShoulder = false;
    public static bool checkRShoulder = false;
    public static int LeftTrigger;
    public static int RightTrigger;
    public static float RightStickX;
    public static float RightStickY;
    public static float LeftStickX;
    public static float LeftStickY;
    public static string LTrigger = "0";
    public static string RTrigger = "0";
    public static string RStickX = "0";
    public static string RStickY = "0";
    public static string LStickX = "0";
    public static string LStickY = "0";

    bool toggletext = false;

    // ------------------------------------
    // Number of controllers to use (MAX: 4)
    // ------------------------------------
    int numControllers = 3;  


    Rect windowRect = new Rect(20, 20, 200, 1500);

    string ContCheck;
    public static string Active = "0";
	// Use this for initialization
	void Start () {
        //n = 3;
        ControllerPlugin.initialize(numControllers);
    }

    private void OnGUI()
    {
        windowRect = GUILayout.Window(0, windowRect, doMyWindow, "Controller Check");
    }

    void doMyWindow(int windowID)
    {
        checkA = GUILayout.Toggle(checkA, "A Button");
        checkB = GUILayout.Toggle(checkB, "B Button");
        checkX = GUILayout.Toggle(checkX, "X Button");
        checkY = GUILayout.Toggle(checkY, "Y Button");
        checkUp = GUILayout.Toggle(checkUp, "Up Button");
        checkDown = GUILayout.Toggle(checkDown, "Down Button");
        checkLeft = GUILayout.Toggle(checkLeft, "Left Button");
        checkRight = GUILayout.Toggle(checkRight, "Right Button");
        checkStart = GUILayout.Toggle(checkStart, "Start Button");
        checkBack = GUILayout.Toggle(checkBack, "Back Button");
        checkLShoulder = GUILayout.Toggle(checkLShoulder, "L Shoulder");
        checkRShoulder = GUILayout.Toggle(checkRShoulder, "R Shoulder");
        GUI.Label(new Rect(10, 280, 80, 25), "Left Trigger");
        LTrigger = GUI.TextField(new Rect(90, 282, 50, 20), LeftTrigger.ToString(), 3);
        GUI.Label(new Rect(10, 305, 80, 25), "Right Trigger");
        RTrigger = GUI.TextField(new Rect(90, 307, 50, 20), RightTrigger.ToString(), 3);
        GUI.Label(new Rect(10, 330, 80, 25), "Left Stick X");
        LStickX = GUI.TextField(new Rect(90, 332, 70, 20), LeftStickX.ToString(), 6);
        GUI.Label(new Rect(10, 355, 80, 25), "Left Stick Y");
        LStickY = GUI.TextField(new Rect(90, 357, 70, 20), LeftStickY.ToString(), 6);
        GUI.Label(new Rect(10, 380, 80, 25), "Right Stick X");
        RStickX = GUI.TextField(new Rect(90, 382, 70, 20), RightStickX.ToString(), 6);
        GUI.Label(new Rect(10, 405, 80, 25), "Right Stick Y");
        RStickY = GUI.TextField(new Rect(90, 407, 70, 20), RightStickY.ToString(), 6);
        GUI.Label(new Rect(10, 430, 80, 25), "Controller");
        ContCheck = GUI.TextField(new Rect(90, 432, 70, 20),"Cont No: " + Active, 20);
    }
    // Update is called once per frame

    public static int ActiveController = 1;

    // Update controller state
    void Controller(int i)
    {
        ControllerPlugin.ControllerUpdate(i);
        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_A)
        {
            checkA = true;
            checkB = false;
            checkX = false;
            checkY = false;
            checkUp = false;
            checkDown = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();
        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_B)
        {
            checkA = false;
            checkB = true;
            checkX = false;
            checkY = false;
            checkUp = false;
            checkDown = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_X)
        {
            checkX = true;
            checkA = false;
            checkB = false;
            checkY = false;
            checkUp = false;
            checkDown = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_Y)
        {
            checkY = true;
            checkA = false;
            checkB = false;
            checkX = false;
            checkUp = false;
            checkDown = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_UP)
        {
            checkUp = true;
            checkDown = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_DOWN)
        {
            checkDown = true;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkLeft = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_LEFT)
        {
            checkLeft = true;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkRight = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_DPAD_RIGHT)
        {
            checkRight = true;
            checkLeft = false;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkStart = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_START)
        {
            checkStart = true;
            checkRight = false;
            checkLeft = false;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkBack = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        else if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_BACK)
        {
            checkBack = true;
            checkStart = false;
            checkRight = false;
            checkLeft = false;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            checkRShoulder = false;
            checkLShoulder = false;
            Active = i.ToString();

        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_LEFT_SHOULDER)
        {
            checkLShoulder = true;
            checkRShoulder = false;
            checkBack = false;
            checkStart = false;
            checkRight = false;
            checkLeft = false;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            Active = i.ToString();
        }

        if (ControllerPlugin.contGetState(i) == CONSTANTS.GAMEPAD_RIGHT_SHOULDER)
        {
            checkRShoulder = true;
            checkLShoulder = false;
            checkBack = false;
            checkStart = false;
            checkRight = false;
            checkLeft = false;
            checkDown = false;
            checkUp = false;
            checkA = false;
            checkB = false;
            checkX = false;
            checkY = false;
            Active = i.ToString();
        }

        // Choose most recently moved controller to be "Active controller"
        if (ControllerPlugin.contLeftTriggerState(i) != 0 || ControllerPlugin.contRightTriggerState(i) != 0  
            || ControllerPlugin.contLeftThumbstickX(i) >= 7849 || ControllerPlugin.contLeftThumbstickX(i) <= -7849 || ControllerPlugin.contLeftThumbstickY(i) >= 7849 || ControllerPlugin.contLeftThumbstickY(i) <= -7849 // LEFT THUMBSTICK
            || ControllerPlugin.contRightThumbstickX(i) >= 8689 || ControllerPlugin.contRightThumbstickX(i) <= -8689 || ControllerPlugin.contRightThumbstickY(i) >= 8689 || ControllerPlugin.contRightThumbstickY(i) <= -8689 // RIGHT THUMBSTICK
            )
        {
            ActiveController = i;
            Active = i.ToString(); // for printing active controller number
        }

        LeftTrigger = ControllerPlugin.contLeftTriggerState(ActiveController);
        RightTrigger = ControllerPlugin.contRightTriggerState(ActiveController);

        LeftStickX = ControllerPlugin.contLeftThumbstickX(ActiveController);
        LeftStickY = ControllerPlugin.contLeftThumbstickY(ActiveController);

        RightStickX = ControllerPlugin.contRightThumbstickX(ActiveController);
        RightStickY = ControllerPlugin.contRightThumbstickY(ActiveController);
    }

    void Update()
    {
        // update all controllers
        for (int i = 1; i <= numControllers; i++)
        {
            Controller(i);
        }
    }
}

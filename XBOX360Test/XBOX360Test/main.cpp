#include "Wrapper.h"
#include <iostream>
#include <iomanip>
#include<Xinput.h>

void output();


XINPUT_STATE state;

class XBOX
{
private:
	XINPUT_STATE controllerState;
	int controllerNum;

public:
	XBOX(int playerNum);
	XINPUT_STATE GetState();
	bool Connected();
};

XBOX *Player[4];
int maxNum;

XBOX::XBOX(int playerNum)
{
	//playerNum = 1;
	controllerNum = playerNum - 1;
}

XINPUT_STATE XBOX::GetState() // get state of controllerl from xinput
{
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	XInputGetState(controllerNum, &controllerState);
	return controllerState;
}

bool XBOX::Connected() // check if controller is connected
{
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	DWORD Result = XInputGetState(controllerNum, &controllerState);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool check(int n)
{
	if (Player[n]->Connected())
	{
		return true;
	}

	else
	{
		return false;
	}
}

void initialize(int n)
{
	for (int i = 1; i <= n; i++)
	{
		Player[i] = new XBOX(i);
	}
	maxNum = n;
}

int contGetState(int n) // get state of buttons from xinput
{
	int num = Player[n]->GetState().Gamepad.wButtons;
	return num;
}

int contLeftTriggerState(int n) // get state of left trigger
{
	int num = Player[n]->GetState().Gamepad.bLeftTrigger;
	return num;
}

int contRightTriggerState(int n) // get state of right trigger
{
	int num = Player[n]->GetState().Gamepad.bRightTrigger;
	return num;
}

/////// GET STATE OF THUMBSTICKS
float contRightThumbstickX(int n)
{
	float num = Player[n]->GetState().Gamepad.sThumbRX;
	return num;
}

float contRightThumbstickY(int n)
{
	float num = Player[n]->GetState().Gamepad.sThumbRY;
	return num;
}

float contLeftThumbstickX(int n)
{
	float num = Player[n]->GetState().Gamepad.sThumbLX;
	return num;
}

float contLeftThumbstickY(int n)
{
	float num = Player[n]->GetState().Gamepad.sThumbLY;
	return num;
}



void ControllerUpdate(int n) // get controller state (not used)
{
	Player[n]->GetState().Gamepad.wButtons;
}


XINPUT_STATE getNumState()
{
	XINPUT_STATE contState;
	ZeroMemory(&contState, sizeof(XINPUT_STATE));

	XInputGetState(0, &contState);

	return contState;
}

int main(int argc, char* argv[]) // debug
{
	initialize(2);
}

void output() // debug
{

	int num = Player[2]->GetState().Gamepad.wButtons;

	std::cout << num << std::endl;

	system("cls");
}



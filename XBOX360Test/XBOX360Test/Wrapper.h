#pragma once
#include "LibSettings.h"
#include <Windows.h>


#ifdef __cplusplus
extern "C"
{
#endif
	//LIB_API int getState();

	LIB_API void initialize(int n);
	LIB_API int contGetState(int n);
	//LIB_API bool check(int n);
	LIB_API void ControllerUpdate(int n);
	LIB_API int contLeftTriggerState(int n);
	LIB_API int contRightTriggerState(int n);
	LIB_API float contRightThumbstickX(int n);
	LIB_API float contRightThumbstickY(int n);
	LIB_API float contLeftThumbstickX(int n);
	LIB_API float contLeftThumbstickY(int n);

}